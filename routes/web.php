<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//auth user only can access
Route::middleware('auth')->group(function () {
    
    //view tweet page
    Route::get('/tweets', 'TweetsController@index')->name('home');
    Route::post('/tweets', 'TweetsController@store');
    Route::patch('/tweets/{tweet}', 'TweetsController@update');
    Route::delete('/tweets/{tweet}', 'TweetsController@destroy');

    // for like and dislike after click button
    Route::post('/tweets/{tweet}/like', 'TweetLikesController@store');
    Route::delete('/tweets/{tweet}/like', 'TweetLikesController@destroy');

    Route::post('/users', 'ProfilesController@store');

    //view profile page
    Route::post('/profiles/{user:username}/follow', 'FollowsController@store')->name('follow');

    // for edit profile
    Route::get('/profiles/{user:username}/edit', 'ProfilesController@edit')->middleware('can:edit,user');

    Route::patch('/profiles/{user:username}', 'ProfilesController@update')->middleware('can:edit,user');

    Route::delete('/profiles/{user:username}', 'ProfilesController@destroy');

    //view explore page
    Route::get('/explore', 'ExploreController');
});

Route::get('/profiles/{user:username}', 'ProfilesController@show')->name('profile');

Auth::routes();
