@auth
    @unless (current_user()->is($user))
        <!-- go to follow page -->
        <form method="POST"
              action="{{ route('follow', $user->username) }}">
              <!-- prevent from getting attack -->
            @csrf

            <button type="submit" class="bg-blue-500 rounded-full shadow py-2 px-4 text-white text-xs">
                <!-- auth user can foloow or unfollow -->
                {{ current_user()->following($user) ? 'Unfollow Me' : 'Follow Me' }}
            </button>
        </form>
    @endunless
@endauth
