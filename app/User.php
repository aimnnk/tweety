<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = []; //attributes that do not want to be mass assignable

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatarAttribute($value)
    {
        return "https://i.pravatar.cc/200?u=".$this->email; //dummy avatar
        // return asset($value); ->error profile
    }

    public function setPasswordAttribute($value)
    {
        //bcrypt pass
        $this->attributes['password'] = bcrypt($value);
    }

    public function timeline()
    {
        //include all of the user's tweets
        //as well as the tweets of everyone
        //they follow, in descending order by date
        
        $friends = $this->follows()->pluck('id');

        //get tweet from following with 5tweet per page
        return Tweet::whereIn('user_id', $friends)->orWhere('user_id', $this->id)->withLikes()->orderByDesc('id')->paginate(5);
    }

    public function tweets()
    {
        //to get the current/latest tweet.. a user can have many tweet
        return $this->hasMany(Tweet::class)->latest();
    }

    public function likes()
    {
        //a user can like many tweet
        return $this->hasMany(Like::class);
    }

    public function path($append = '')
    {
        //path represent user 
        $path = route('profile', $this->username);

        //return to user
        return $append ? "{$path}/{$append}" : $path;
    }
}
