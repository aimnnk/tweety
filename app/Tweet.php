<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    // inheritance
    use Likable; 

    protected $guarded = []; //specifies which fields are not mass assignable

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function path()
    {
        return '/tweets' . $this->user_id;
    }

    public function setUserAttribute($user)
    {
        $this->attributes['user_id'] = (User::firstOrCreate([
            'name' => $user
        ]))->id;
    }
}
