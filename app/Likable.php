<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;

trait Likable
{
    // tweet::withlike()->all()
    public function scopeWithLikes(Builder $query)
    {
        // like & dislike
        $query->leftJoinSub(
            'select tweet_id, sum(liked) likes, sum(!liked) dislikes from likes group by tweet_id',
            'likes',
            'likes.tweet_id',
            'tweets.id'
        );
    }

    public function isLikedBy(User $user)
    {
        // accept like by yser
        return (bool) $user->likes->where('tweet_id', $this->id)->where('liked', true)->count();
    }

    public function isDislikedBy(User $user)
    {
        // accpt dislike by uuser
        return (bool) $user->likes->where('tweet_id', $this->id)->where('liked', false)->count();
    }

    public function likes()
    {
        // user can have many like
        return $this->hasMany(Like::class);
    }



    // accept user
    public function dislike($user = null)
    {
        // user can have many idslike
        return $this->like($user, false);
    }

    public function like($user = null, $liked = true)
    {
        $this->likes()->updateOrCreate(
            [
                'user_id' => $user ? $user->id : auth()->id(),
            ],
            [
                'liked' => $liked,
            ]
        );
    }
}
