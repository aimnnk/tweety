<?php

namespace App\Http\Controllers;

use App\Tweet;

class TweetsController extends Controller
{
    public function index()
    {
        //return to home/index after click button tweet
        return view('tweets.index', [
            'tweets' => auth()
                ->user()
                ->timeline(), 
        ]);
    }

    public function store()
    {
        //validate tweet
        // $attributes = request()->validate([
        //     'body' => 'required|max:255',
        // ]);
        $tweet = $attributes = $this->validateRequest();

        //create the tweet
        Tweet::create([
            'user_id' => auth()->id(),
            'body' => $attributes['body'],
        ]);

        return redirect()->route('home'); //after tweet return to home
    }

    public function update(Tweet $tweet)
    {
        $tweet->update($this->validateRequest());

        return redirect()->route('home');
    }

    public function destroy(Tweet $tweet)
    {
        $tweet->delete();

        return redirect()->route('home');
    }

    //refactor validate
    public function validateRequest()
    {
        return request()->validate([
                'user_id' => 'required',
                'body' => 'required|max:150'
        ]);
    }

    public function __construct()
    {
        $this->middleware('auth');
    }
}
