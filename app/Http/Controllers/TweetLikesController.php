<?php

namespace App\Http\Controllers;

use App\Tweet;

class TweetLikesController extends Controller
{
    public function store(Tweet $tweet)
    {   
        // store like tweet
        $tweet->like(current_user());

        return back(); // return tweet
    }

    public function destroy(Tweet $tweet)
    {
        // store dislike tweet
        $tweet->dislike(current_user());

        return back();
    }
}
