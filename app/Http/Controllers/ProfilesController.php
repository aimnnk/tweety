<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Validation\Rule;

class ProfilesController extends Controller
{
    public function store()
    {
        $user = User::create(request()->only([
            'username', 'name', 'email', 'password'
        ]));
    }

    public function show(User $user)
    {
        //show user profile
        return view('profiles.show', [
            'user' => $user,
            'tweets' => $user->tweets()->withLikes()->paginate(5), //show like with 5tweets per page
        ]);
    }

    public function edit(User $user)
    {
        //show edit profile, sent through user
        return view('profiles.edit', compact('user'));
    }

    public function update(User $user)
    {
        //validate the update
        $attributes = request()->validate([
            'username' => ['string','required','max:255','alpha_dash',Rule::unique('users')->ignore($user),], //username must be unique
            'name' => ['string', 'required', 'max:255'],
            'avatar' => ['image'],
            'email' => ['string','required','email','max:255',Rule::unique('users')->ignore($user),],
            'password' => ['string','required','min:8','max:255','confirmed',],
        ]);

        //for save avatar
        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }

        $user->update($attributes); //update user given attri

        return redirect($user->path()); //return to user profile
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('home');
    }
}
