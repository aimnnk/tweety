<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); //auth user only
    }

    public function index()
    {
        return view('home', [
            'tweets' => auth()->user()->timeline() //will go to timeline as home
        ]);
    }
}
