<?php

namespace App\Http\Controllers;

use App\User;

class ExploreController extends Controller
{
    //handles a single action
    public function __invoke()
    {
        return view('explore', [
            'users' => User::paginate(5), //only 5person per page
        ]);
    }
}
