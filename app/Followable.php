<?php

namespace App;

// inherit class
trait Followable
{
    public function follow(User $user)
    {
        //can follow any user
        return $this->follows()->save($user);
    }

    public function unfollow(User $user)
    {
        //can unfollow any user
        return $this->follows()->detach($user);
    }

    //$user->toggleFollow($otherUser)
    public function toggleFollow(User $user)
    {
        $this->follows()->toggle($user);
    }

    public function following(User $user)
    {
        //create a new relationship with follow
        return $this->follows()->where('following_user_id', $user->id)->exists();
    }

    public function follows()
    {
        //follow a user
        return $this->belongsToMany(
            User::class,
            'follows',
            'user_id',
            'following_user_id'
        );
    }
}
