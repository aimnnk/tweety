<?php

namespace Tests\Unit;

use App\Tweet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TweetRecordTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_id_is_recorded()
    {
        Tweet::create([
            'user_id' => 2,
            'body' => 'cool tweet'
        ]);

        $this->assertCount(2, Tweet::all());
    }
}
