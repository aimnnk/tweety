<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserAvatarTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_avatar_is_not_required()
    {
        User::firstOrCreate([
            'username' => 'aimnnk',
            'name' => 'aiman khalid',
            'email' => 'aimnnk@test.com',
            'password' => 'pop12345',
        ]);

        $this->assertCount(2, User::all());
    }
}
