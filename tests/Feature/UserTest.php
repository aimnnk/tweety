<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_user_can_view_index_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $response->assertSee('Tweety');
    }

    /** @test */
    public function a_user_can_be_created()
    {
        // $user = factory(User::class)->create();
        // $this->actingAs($user);

        $response = $this->post('/users', $this->data());

        $user = User::all();

        $this->assertCount(1, $user);

        // $this->assertDatabaseHas('users',[
        //     'username' => $user->username,
        //     'name' => $user->name,
        //     'email' => $user->email,
        //     'password' => $user->password,
        // ]);
    }

    /** @test */
    public function a_username_is_required()
    {
        $response = $this->post('/users', array_merge($this->data(), ['username' => '']));

        $response->assertSessionHasErrors('username');
    }

    /** @test */
    public function a_user_can_be_updated()
    {
        $this->post('/users', $this->data());

        $user = User::first();

        $response = $this->patch('/profiles/' .$user->id, [
            'username' => 'aimnnk',
            'name' => 'aiman',
            'email' => 'aiman@test.com',
            'password' => 'abc12345',
        ]);

        // $response->assertStatus(302);

        $this->assertEquals('aimnnk', User::first()->username);
        $this->assertEquals('aiman', User::first()->name);
        $this->assertEquals('aiman@test.com', User::first()->email);
        $this->assertEquals('abc12345', User::first()->password);

    }
    
    /** @test */
    public function a_user_can_be_deleted()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $user = User::first();

        $response = $this->delete('/users/' .$user->id);

        $this->assertCount(2, User::all()); 
    }

    private function data()
    {
        return [
            'username' => 'aimnnk',
            'name' => 'aiman khalid',
            'email' => 'aimnnk@test.com',
            'password' => 'Pop12345',
        ];
    }
}
