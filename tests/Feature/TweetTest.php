<?php

namespace Tests\Feature;

use App\Tweet;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TweetTest extends TestCase
{
    use RefreshDatabase;

     /** @test */
    public function a_user_can_add_new_tweet()
    {
        // $user = factory(User::class)->create();
        // $this->actingAs($user);

        $response = $this->post('/tweets', $this->data());

        $tweet = Tweet::first();

        $this->assertCount(1, Tweet::all());
        
        $this->assertDatabaseHas('tweets',[
            'user_id' => $tweet->user_id,
            'body' => $tweet->body
        ]);
    }

    /** @test */
    public function a_body_is_required()
    {
        $response = $this
            ->post('/tweets', array_merge($this->data(), ['body' => null]));

        // $response->assertSessionHasErrors('body');
        $this->assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function a_tweet_can_be_updated()
    {
        $this->post('/tweets', $this->data());

        $tweet = Tweet::first();

        // $response = $this->patch('/tweets/' .$tweet->id, [
        //     'user_id' => 1,
        //     'body' => 'hello twitter'
        // ]);

        $response = $this->patch($tweet->path(), [
            'user_id' => 1,
            'body' => 'hello twitter'
        ]);

        // $this->assertEquals(1, Tweet::first()->user_id);
        // $this->assertEquals('hello twitter', Tweet::first()->body);
        

        $this->assertDatabaseHas('tweets', [
            'id' => $tweet->id,
            'user_id' => 1,
            'body' => 'hello twitter',
        ]);
    }

    /** @test */
    public function a_tweet_can_be_deleted()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $tweet = Tweet::first();

        $response = $this->delete('/tweets/' .$tweet->id);

        $this->assertCount(0, Tweet::all());
        $this->assertDatabaseMissing('tweets', [
            'user_id' => $tweet->user_id,
            'body' => $tweet->body,
        ]);
    }

    /** @test */
    public function a_new_user_is_automatically_added()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $tweet = Tweet::first();
        $user = User::first();

        $this->assertCount(3, User::all());
    }

    private function data()
    {
        return [
            'user_id' => 'aimnnk',
            'body' => 'new tweet'
        ];
    }
}
